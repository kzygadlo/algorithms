﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Iteration
{
    public static class MatrixZeroes
    {
        static int tempSign = -1;
        public static int[,] Run(int[,] matrix)
        {
            for(int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int column = 0; column < matrix.GetLength(1); column++)
                {
                    if (matrix[row, column] == 0)
                    {
                        MarkRowAndColumnWithZeroValues(matrix, row, column);
                    }
                }
            }   

            return ConvertToZeroValues(matrix);
        }

        private static int[,] ConvertToZeroValues(int[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int column = 0; column < matrix.GetLength(1); column++)
                {
                    if (matrix[row, column] == tempSign)
                    {
                        matrix[row, column] = 0;
                    }
                }
            }

            return matrix;
        }

        private static void MarkRowAndColumnWithZeroValues(int[,] matrix, int rowIndex, int columnIndex)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int column = 0; column < matrix.GetLength(1); column++)
                {
                    if (row == rowIndex || column == columnIndex)
                    {
                        matrix[row, column] = tempSign;
                    }
                }
            }
        }
    }
}
