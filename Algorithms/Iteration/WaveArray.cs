﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Iteration
{
    public class WaveArray
    {
        public static int[] Sort(int[] arrayToSort)
        {
            var sortedArray = arrayToSort.OrderBy(x => x).ToArray();
            WaveSort(sortedArray);
            return sortedArray;
        }

        private static void WaveSort(int[] arrayToSort)
        {
            int index = 0;

            foreach (int item in arrayToSort)
            {
                if (index%2 == 0 && index < arrayToSort.Length - 1) {
                    int secondNumber = arrayToSort[index + 1];
                    arrayToSort[index + 1] = arrayToSort[index];
                    arrayToSort[index] = secondNumber;
                }
                index++;
            }
        }
    }
}
