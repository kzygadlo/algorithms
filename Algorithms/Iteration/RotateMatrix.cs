﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleJuly.Iteration
{
    public static class RotateMatrix
    {
        public static int[,] RunRotation(int[,] matrix)
        {
            int size = matrix.GetLength(0);

            SwapRowsWithColumns(matrix, size);
            SwapRowsValues(matrix, size);
            return matrix;
        }

        public static void SwapRowsValues(int[,] matrix, int size)
        {
            for (int row = 0; row < size; row++)
            {
                for (int column = 0; column < size / 2; column++)
                {
                    SwitchValues(matrix, row, column, row, size - column - 1);
                }
            }
        }   
                        
        private static void SwapRowsWithColumns(int[,] matrix, int size)
        {
            for (int column = 0; column < size; column++)
            {
                for (int row = column; row < size; row++)
                {
                    SwitchValues(matrix, row, column, column, row);
                }
            }
        }

        private static void SwitchValues(int[,] matrix, int row1, int column1, int row2, int column2)
        {
            int tempValue = matrix[row1, column1];
            matrix[row1, column1] = matrix[row2, column2];
            matrix[row2, column2] = tempValue;
        }
    }
}
