﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.MathematicalAlgorithms
{
    public static class GreatestCommonDivisor
    {
        public static int Find(int firstNum, int secondNum)
        {
            if (firstNum <= 0 || secondNum <= 0) throw new Exception("The power value needs to be higher than 0.");

            int theLower = firstNum < secondNum ? firstNum : secondNum;
            int result = 0;

            for (int i = 1; i <= theLower; i++)
            {
                if (firstNum % i == 0 && secondNum % i == 0)
                {
                    result = i;
                }
            }
            return result;
        }
    }
}
