﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.MathematicalAlgorithms
{
    public static class SquareRootOfInteger
    {
        public static int GetSquareRoot(int input)
        {
            if (input < 0) throw new Exception("The value cannot be negative.");

            return Convert.ToInt32(Math.Sqrt(input));
        }
    }
}
