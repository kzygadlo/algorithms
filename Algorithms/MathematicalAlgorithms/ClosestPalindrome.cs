﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.MathematicalAlgorithms
{
    public static class ClosestPalindrome
    {
        public static string Find(string startingPoint)
        {
            int valueToExaminate = Convert.ToInt32(startingPoint);

            int lowerPalindrome = findTheLowerPalindrome(valueToExaminate);
            int upperPalindrome = findTheUpperPalindrome(valueToExaminate);

            int closestPalindrome = valueToExaminate - lowerPalindrome > upperPalindrome - valueToExaminate ? upperPalindrome : lowerPalindrome;
            return closestPalindrome.ToString();
        }

        private static int findTheLowerPalindrome(int valueToExaminate)
        {
            for (int i = valueToExaminate - 1; i > 0; i--)
            {
                bool palindrome = isPalindrome(i.ToString());
                if (palindrome) return i;
            }
            return -1;
        }

        private static int findTheUpperPalindrome(int valueToExaminate)
        {
            do
            {
                if (isPalindrome((++valueToExaminate).ToString()))
                {
                    return valueToExaminate;
                }
            }
            while (!isPalindrome(valueToExaminate.ToString()));

            return -1;
        }

        private static bool isPalindrome(string value)
        {
            var v1 = value.ToCharArray();
            var v2 = value.ToCharArray();
            Array.Reverse(v2);

            return v1.SequenceEqual(v2);
        }
    }
}
