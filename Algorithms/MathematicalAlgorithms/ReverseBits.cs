﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.MathematicalAlgorithms
{
    public static class ReverseBits
    {
        public static int ReversBits(this int value)
        {
            if (!IsPositive(value)) throw new Exception("The value cannot be negative.");

            var bitsArray = Convert.ToString(value, 2).ToCharArray();

            Array.Reverse(bitsArray);

            return Convert.ToInt32(new string(bitsArray), 2);
        }

        private static bool IsPositive(int value)
        {
            return value >= 0;
        }
    }
}
