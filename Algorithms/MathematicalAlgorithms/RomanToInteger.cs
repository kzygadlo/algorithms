﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.MathematicalAlgorithms
{
    public static class RomanToInteger
    {
        private static IDictionary<char, ushort> _mapping = new Dictionary<char, ushort>()
        {
            {'I', 1 },
            {'V', 5 },
            {'X', 10 },
            {'L', 50 },
            {'C', 100 },
            {'D', 500 },
            {'M', 1000 },
        };

        public static int Convert(string value)
        {
            if (!IsRoman(value))
            {
                throw new Exception($"Provided value {value} is not a Roman value.");
            }
            var result = 0;
            var i = 0;
            foreach (char c in value)
            {
                int convertedValue = _mapping.Where(x => x.Key == c).Select(x => x.Value).FirstOrDefault();
                if (i > 0 && convertedValue > _mapping.Where(x => x.Key == value[i - 1]).Select(x => x.Value).FirstOrDefault())
                {
                    result = convertedValue - result;
                }
                else
                {
                    result += convertedValue;
                }
                i++;
            }
            return result;
        }

        private static bool IsRoman(string value)
        {
            foreach (char c in value)
            {
                if (!_mapping.Any(x => x.Key == c)) return false;
            }
            return true;
        }
    }
}
