﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.MathematicalAlgorithms
{
    public class PowerFunction
    {
        public static int Calculate(int value, int power)
        {
            if (power <= 0) throw new Exception("The power value needs to be higher than 0.");

            int result = value;

            for (int i = 1; i < power; i++)
            {
                result *= value;
            }
            return result;
        }
    }
}
