﻿using ConsoleJuly.Iteration;
using ConsoleJuly.MathematicalAlgorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleJulyTest.Iteration
{
    internal class RotateMatrix_Test
    {
        [Test]
        public void Rotate_Matrix_1()
        {
            // Arrange
            int[,] matrix = new int[,] {
              { 7, 8, 9},
              { 1, 2, 3},
              { 4, 5, 6}
            };

            int[,] expected = new int[,] {
              { 4, 1, 7},
              { 5, 2, 8},
              { 6, 3, 9}
            };

            // Act
            int[,] result = RotateMatrix.RunRotation(matrix);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Rotate_Matrix_2()
        {
            // Arrange
            int[,] matrix = new int[,] {
              { 1, 2, 3, 4},
              { 5, 6, 7, 8},
              { 9, 10, 11, 12},
              { 13, 14, 15, 16}
            };

            int[,] expected = new int[,] {
              { 13, 9, 5, 1},
              { 14, 10, 6, 2},
              { 15, 11, 7, 3},
              { 16, 12, 8, 4}
            };

            // Act
            int[,] result = RotateMatrix.RunRotation(matrix);

            // Assert
            Assert.AreEqual(expected, result);
        }
    }
}
