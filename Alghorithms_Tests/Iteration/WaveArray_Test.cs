﻿using Algorithms.Iteration;
using Algorithms.Iteration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsTests.Iteration
{
    internal class WaveArray_Test
    {
        [Test]
        public void SortWaveArray_1()
        {
            // Arrange
            int[] arrayToSort = new int[] { 5, 2, 9, 3, 2 };

            // Act
            int[] result = WaveArray.Sort(arrayToSort);

            // Assert
            CollectionAssert.AreEqual(new int[] { 2, 2, 5, 3, 9 }, result);
        }

        [Test]
        public void SortWaveArray_2()
        {
            // Arrange
            int[] arrayToSort = new int[] { 3, 2, 9, 6, 4, 1 };

            // Act
            int[] result = WaveArray.Sort(arrayToSort);

            // Assert
            CollectionAssert.AreEqual(new int[] { 2, 1, 4, 3, 9, 6 }, result);
        }

        [Test]
        public void SortWaveArray_3()
        {
            // Arrange
            int[] arrayToSort = new int[] { 4, 2, 9, 1, 21, 43, 24 };

            // Act
            int[] result = WaveArray.Sort(arrayToSort);

            // Assert
            CollectionAssert.AreEqual(new int[] { 2, 1, 9, 4, 24, 21, 43 }, result);
        }
    }
}
