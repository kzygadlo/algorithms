﻿using Algorithms.Iteration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsTest.Iteration
{
    internal class SpiralMatrix_Test
    {
        [Test]
        public void SpiralMatrix_1()
        {
            // Arrange
            int[,] matrix = new int[,] {
                    { 1, 2 },
                    { 3, 4 },
            };

            // Act
            int[] result = SpiralMatrix.Convert(matrix);

            // Assert
            CollectionAssert.AreEqual(new int[] { 1, 2, 4, 3 }, result);
        }

        [Test]
        public void SpiralMatrix_2()
        {
            // Arrange
            int[,] matrix = new int[,] {
                    { 5, 8, 3 },
                    { 3, 8, 6 },
                    { 4, 7, 3 },
            };

            // Act
            int[] result = SpiralMatrix.Convert(matrix);

            // Assert
            CollectionAssert.AreEqual(new int[] { 5, 8, 3, 6, 3, 7, 4, 3, 8 }, result);
        }
    }
}
