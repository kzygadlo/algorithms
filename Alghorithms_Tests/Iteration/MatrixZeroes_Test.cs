﻿using Algorithms.Iteration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsTest.Iteration
{
    public class MatrixZeroes_Test
    {
        [Test]
        public void MatrixZeroes_1()
        {
            // Arrange
            int[,] matrix = new int[,] {
              { 0, 1},
              { 1, 1}
            };

            int[,] expected = new int[,] {
              { 0, 0},
              { 0, 1}
            };

            // Act
            int[,] result = MatrixZeroes.Run(matrix);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void MatrixZeroes_2()
        {
            // Arrange
            int[,] matrix = new int[,] {
              { 1, 1, 1},
              { 1, 0, 1},
              { 1, 1, 1}
            };

            int[,] expected = new int[,] {
              { 1, 0, 1},
              { 0, 0, 0},
              { 1, 0, 1}
            };

            // Act
            int[,] result = MatrixZeroes.Run(matrix);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}
