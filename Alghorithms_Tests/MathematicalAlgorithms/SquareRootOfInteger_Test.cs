﻿using Algorithms.MathematicalAlgorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsTest.MathematicalAlgorithms
{
    internal class SquareRootOfInteger_Test
    {
        [Test]
        public void GetSquareOf_4()
        {
            // Arrange
            int input = 6;

            // Act
            int result = SquareRootOfInteger.GetSquareRoot(input);

            // Assert
            Assert.AreEqual(result, 2);
        }

        [Test]
        public void GetSquareOf_11()
        {
            // Arrange
            int input = 11;

            // Act
            int result = SquareRootOfInteger.GetSquareRoot(input);

            // Assert
            Assert.AreEqual(result, 3);
        }

        [Test]
        public void GetSquareOf_55()
        {
            // Arrange
            int input = 55;

            // Act
            int result = SquareRootOfInteger.GetSquareRoot(input);

            // Assert
            Assert.AreEqual(result, 7);
        }

        [Test]
        public void NegativeNumberException()
        {
            int romanToTest = -1;

            var ex = Assert.Throws<Exception>(() => SquareRootOfInteger.GetSquareRoot(romanToTest));

            Assert.AreEqual(ex.Message, $"The value cannot be negative.");
        }
    }
}
