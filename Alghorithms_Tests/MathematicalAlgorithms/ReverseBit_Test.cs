﻿using Algorithms.MathematicalAlgorithms;

namespace AlgorithmsTest.MathematicalAlgorithms
{
    internal class ReverseBit_Test
    {

        [Test]
        public void BitReversingOf_13()
        {
            // Arrange
            int intToConvert = 13;

            // Act
            int result = intToConvert.ReversBits();

            // Assert
            Assert.AreEqual(result, 11);
        }

        [Test]
        public void BitReversingOf_6()
        {
            // Arrange
            int intToConvert = 6;

            // Act
            int result = intToConvert.ReversBits();

            // Assert
            Assert.AreEqual(result, 3);
        }

        [Test]
        public void ThrowsErrorWhenNegative()
        {
            // Arrange 
            int intToConvert = -5;

            // Act
            var ex = Assert.Throws<Exception>(() => intToConvert.ReversBits());

            // Assert
            Assert.AreEqual(ex.Message, "The value cannot be negative.");
        }

    }
}
