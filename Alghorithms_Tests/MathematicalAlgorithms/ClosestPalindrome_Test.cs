﻿using Algorithms.MathematicalAlgorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsTest.MathematicalAlgorithms
{
    internal class ClosestPalindrome_Test
    {
        [Test]
        public void FindTheClosestPalindromOf_99()
        {
            // Arrange
            string startingPoint = "99";

            // Act
            string theClosesPalindrome = ClosestPalindrome.Find(startingPoint);

            // Assert
            Assert.AreEqual("101", theClosesPalindrome);
        }

        [Test]
        public void FindTheClosestPalindromOf_59()
        {
            // Arrange
            string startingPoint = "59";

            // Act
            string theClosesPalindrome = ClosestPalindrome.Find(startingPoint);

            // Assert
            Assert.AreEqual("55", theClosesPalindrome);
        }
    }
}
