using Algorithms.MathematicalAlgorithms;
using Newtonsoft.Json.Linq;

namespace AlgorithmsTest.MathematicalAlgorithms
{
    public class RomanToInteger_Test
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void Test_VII()
        {
            string romanToTest = "VII";

            int convertedToInt = RomanToInteger.Convert(romanToTest);

            Assert.AreEqual(7, convertedToInt);
        }

        [Test]
        public void Test_IV()
        {
            string romanToTest = "IV";

            int convertedToInt = RomanToInteger.Convert(romanToTest);

            Assert.AreEqual(4, convertedToInt);
        }

        [Test]
        public void Test_XC()
        {
            string romanToTest = "XC";

            int convertedToInt = RomanToInteger.Convert(romanToTest);

            Assert.AreEqual(90, convertedToInt);
        }

        [Test]
        public void Test_XVII()
        {
            string romanToTest = "XVII";

            int convertedToInt = RomanToInteger.Convert(romanToTest);

            Assert.AreEqual(17, convertedToInt);
        }

        [Test]
        public void NotRomanValue_GH()
        {
            string romanToTest = "GH";

            var ex = Assert.Throws<Exception>(() => RomanToInteger.Convert(romanToTest));

            Assert.AreEqual(ex.Message, $"Provided value {romanToTest} is not a Roman value.");
        }
    }
}