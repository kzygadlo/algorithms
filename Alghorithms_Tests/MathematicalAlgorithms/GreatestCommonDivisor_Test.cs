﻿using Algorithms.MathematicalAlgorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsTest.MathematicalAlgorithms
{
    internal class GreatestCommonDivisor_Test
    {
        [Test]
        public void GreatestCommonDivisorOf_54_and_24()
        {
            // Arrange
            int theFirstNumber = 54;
            int theSecondNumber = 24;

            // Act
            int greatestCommonDivisor = GreatestCommonDivisor.Find(theFirstNumber, theSecondNumber);

            // Assert
            Assert.AreEqual(6, greatestCommonDivisor);
        }

        [Test]
        public void NegativeOrZeroPowerException()
        {
            // Arrange
            int theFirstNumber = 54;
            int theSecondNumber = 0;

            var ex = Assert.Throws<Exception>(() => GreatestCommonDivisor.Find(theFirstNumber, theSecondNumber));

            Assert.AreEqual(ex.Message, $"The power value needs to be higher than 0.");
        }
    }
}
