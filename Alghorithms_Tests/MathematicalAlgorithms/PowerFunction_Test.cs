﻿using Algorithms.MathematicalAlgorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsTest.MathematicalAlgorithms
{
    public class PowerFunction_Test

    {
        [Test]
        public void FourToThePowerOfTwo()
        {
            // Arrange
            int theNumber = 4;
            int thePower = 3;

            // Act
            int result = PowerFunction.Calculate(theNumber, thePower);

            // Assert
            Assert.AreEqual(64, result);
        }

        [Test]
        public void MinsSevenToThePowerOfThree()
        {
            // Arrange
            int theNumber = -7;
            int thePower = 3;

            // Act
            int result = PowerFunction.Calculate(theNumber, thePower);

            // Assert
            Assert.AreEqual(-343, result);
        }

        [Test]
        public void NegativeOrZeroPowerException()
        {
            // Arrange
            int theNumber = 4;
            int thePower = -1;

            var ex = Assert.Throws<Exception>(() => PowerFunction.Calculate(theNumber, thePower));

            Assert.AreEqual(ex.Message, "The power value needs to be higher than 0.");
        }
    }
}
